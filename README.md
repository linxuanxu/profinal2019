# proFinal2019 #

## 1. Basical operations ##

In the MainActivity, users click Yelp Fusion button to search businesses. In BusinessSearchActivity, users can search businesses according to different conditions.

Then users can clikc the item of recyclerview to get details of business. 

In BusinessDetailActivity, users can get reviews (by yelp api) and add this business to database.

In the MainActivity, users click Saved Business button to check the saved businesses. Users can go to the detail of business and click delete button to delete this business from saved businesses.

In the MainActivity, users click settings and go to SettingActivity to change the background color. There are different colors that users can choose.

## 2. Uses local storage and must use a Room database. ##

I used Room to implement a set of database operations like insert, update, delete, and so on about Business (it is a entity class).

And I used @embedded annotation to embed the Location class. 

Besides, I created repository class to encalpsulate database.

## 3. Uses SharedPreferences to allow the user to change their prefs. ##

There is a SettingActivity, and users can use SharedPreferences to change the setting of this APP (the background color).

## 4. Connects to a remote API. ##

I used Yelp Fusion API and users can search businesses based on conditions and search up to 3 reviews of every business.

Users can also save the business.

## 5. Provides at least one RecyclerView for viewing data in a list. ##

I used RecyclerView many times. For example, when the app display businesses that users use Yelp Fusion API to search, it used RecyclerView.

## 6. Uses the JetPack M-V-VM architecture with reactive 2-way communication between the view and model. ##

I used SavedBusinessViewModel to display the business list in SavedBusinessActivity. And when there is a change (delete or add a business), the model will be changed.

And users can see the current saved businesses.
