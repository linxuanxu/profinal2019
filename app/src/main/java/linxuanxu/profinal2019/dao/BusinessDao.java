package linxuanxu.profinal2019.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import linxuanxu.profinal2019.model.Business;

@Dao
public interface BusinessDao {
    @Query("select * from business_table where id=:id")
    Business getBusinessById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBusiness(Business word);

    @Query("select * from business_table")
    LiveData<List<Business>> getAllBusinesses();

    @Update
    void updateBusiness(Business b);

    @Delete
    void deleteBusiness(Business b);

    @Query("delete from business_table")
    void deleteAll();
}
