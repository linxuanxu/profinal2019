package linxuanxu.profinal2019.util;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class YelpAPI {
    private String key;
    private OkHttpClient client;

    public YelpAPI(String key){
        this.key=key;
        client=new OkHttpClient();
    }

    public JSONObject getBusiness(String term, String location, String price){
        Request request = new Request.Builder()
                .url("https://api.yelp.com/v3/businesses/search?term=" + term + "&location=" + location + "&sort_by=rating&price="+price+"")
                .get()
                .addHeader("authorization", "Bearer "+key)
                .addHeader("cache-control", "no-cache")
                .build();
        JSONObject res=null;
        try {
            Response response2 = client.newCall(request).execute();
            res = new JSONObject(response2.body().string().trim());
        } catch (IOException e) {
            Log.e("error","JSON exception in API.");
            e.printStackTrace();
        } catch (JSONException e){
            Log.e("error","IO exception in API.");
            e.printStackTrace();
        }
        return res;
    }

    public JSONObject getReview(String id){
        Request request = new Request.Builder()
                .url("https://api.yelp.com/v3/businesses/"+id+"/reviews")
                .get()
                .addHeader("authorization", "Bearer "+key)
                .addHeader("cache-control", "no-cache")
                .build();
        JSONObject res=null;
        try {
            Response response2 = client.newCall(request).execute();
            res = new JSONObject(response2.body().string().trim());
        } catch (IOException e) {
            Log.e("error","JSON exception in API.");
            e.printStackTrace();
        } catch (JSONException e){
            Log.e("error","IO exception in API.");
            e.printStackTrace();
        }
        return res;
    }
}
