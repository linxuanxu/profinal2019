package linxuanxu.profinal2019.database;


import android.arch.lifecycle.LiveData;
import android.content.Context;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import linxuanxu.profinal2019.model.Business;


public class BusinessRepository {
    private static BusinessRepository ourInstance;

    public LiveData<List<Business>> business;
    private BusinessDatabase db;
    private Executor executor = Executors.newSingleThreadExecutor();

    public static BusinessRepository getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new BusinessRepository(context);
        }
        return ourInstance;
    }

    private BusinessRepository(Context context) {
        db = BusinessDatabase.getInstance(context);
        business = getAllBusinesses();
    }

    private LiveData<List<Business>> getAllBusinesses() {
        return db.getBusinessDao().getAllBusinesses();
    }

    public Business getBusinessById(String id){
        return db.getBusinessDao().getBusinessById(id);
    }

    public void updateBusiness(final Business b) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.getBusinessDao().updateBusiness(b);
            }
        });
    }

    public void insertBusiness(final Business b) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.getBusinessDao().insertBusiness(b);
            }
        });
    }

    public void deleteBusiness(final Business b) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.getBusinessDao().deleteBusiness(b);
            }
        });
    }

    public void deleteAllBusinesses() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.getBusinessDao().deleteAll();
            }
        });
    }
}
