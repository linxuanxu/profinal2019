package linxuanxu.profinal2019.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import linxuanxu.profinal2019.dao.BusinessDao;
import linxuanxu.profinal2019.model.Business;

@Database(entities = {Business.class}, version = 1, exportSchema = false)
public abstract class BusinessDatabase extends RoomDatabase {
    public abstract BusinessDao getBusinessDao();

    private static volatile BusinessDatabase INSTANCE;

    public static BusinessDatabase getInstance(final Context con){
        if(INSTANCE==null){
            synchronized(BusinessDatabase.class){
                if(INSTANCE==null){
                    INSTANCE= Room.databaseBuilder(con.getApplicationContext(), BusinessDatabase.class,"task_database").build();
                }
            }
        }
        return INSTANCE;
    }

    public void close(){
        INSTANCE=null;
    }
}
