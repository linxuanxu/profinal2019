package linxuanxu.profinal2019.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import linxuanxu.profinal2019.R;
import linxuanxu.profinal2019.adapter.ReviewAdapter;
import linxuanxu.profinal2019.database.BusinessRepository;
import linxuanxu.profinal2019.model.Business;
import linxuanxu.profinal2019.model.Review;
import linxuanxu.profinal2019.util.YelpAPI;

public class BusinessDetailActivity extends AppCompatActivity {

    private Business business;

    @BindView(R.id.business_name)
    TextView nameText;
    @BindView(R.id.business_price)
    TextView priceText;
    @BindView(R.id.business_rating)
    TextView ratingText;
    @BindView(R.id.business_location)
    TextView locationText;
    @BindView(R.id.business_review_count)
    TextView reviewCountText;
    @BindView(R.id.business_phone)
    TextView phoneText;

    @BindView(R.id.review_button)
    Button button;

    @BindView(R.id.review_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.review_hint)
    TextView hintText;

    private YelpAPI api;
    private String key;

    private List<Review> reviewsList=new ArrayList<>();
    private ReviewAdapter reviewAdapter;

    private BusinessRepository repository;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_detail);
        Toolbar toolbar = findViewById(R.id.toolbar_business);
        toolbar.setTitle("Business detail");
        setSupportActionBar(toolbar);
        business=(Business) getIntent().getSerializableExtra("business");
        ButterKnife.bind(this);
        setColor();
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getReview();
            }
        });
        repository=BusinessRepository.getInstance(this);
        initAPI();
        initRecyclerView();
        populate();
    }

    private void initAPI(){
        key=getResources().getString(R.string.yelp_fusioin_key);
        if(key!=null){
            api=new YelpAPI(key);
        }
    }

    private void initRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        reviewAdapter=new ReviewAdapter(this,reviewsList);
        recyclerView.setAdapter(reviewAdapter);
    }

    private void populate(){
        //handle with exception
        if(business==null){
            Toast.makeText(BusinessDetailActivity.this,
                    "Business is not initialized." ,
                    Toast.LENGTH_LONG
            ).show();
            return;
        }
        if(business.getName()!=null)
            nameText.setText("Name: "+business.getName());
        if(business.getPrice()!=null)
            priceText.setText("Price: "+business.getPrice());
        if(business.getRating()!=0)
            ratingText.setText("Rating: "+business.getRating());
        if(business.getLocation()!=null)
            locationText.setText("Location: "+business.getLocation().toString());
        if(business.getReview_count()!=0)
            reviewCountText.setText("Review count: "+business.getReview_count());
        if(business.getPhone()!=null)
            phoneText.setText("Phone: "+business.getPhone());
    }

    private void getReview(){
        //handle with exception
        try {
            if(business.getId()==null||business.getId().equals("")){
                Toast.makeText(BusinessDetailActivity.this,
                        "Something wrong about business id." ,
                        Toast.LENGTH_LONG
                ).show();
                return;
            }
            new ReviewTask(BusinessDetailActivity.this).execute(new String[]{business.getId()});
        }catch (Exception e){
            Toast.makeText(BusinessDetailActivity.this,
                    "Something wrong about reviews." ,
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    private class ReviewTask extends AsyncTask<String, Void, List<Review>> {
        private ProgressDialog progressDialog;
        private WeakReference<BusinessDetailActivity> activityWeakReference;

        ReviewTask(BusinessDetailActivity context){
            activityWeakReference=new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(BusinessDetailActivity.this);
            progressDialog.setTitle("Searching...");
            progressDialog.setMessage("One moment please...");
            progressDialog.setCancelable(true);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ReviewTask.this.cancel(true);
                            progressDialog.dismiss();
                        }
                    });
            progressDialog.show();
        }

        @Override
        protected List<Review> doInBackground(String... params) {
            if(api==null){
                return null;
            }
            try {
                JSONObject result= api.getReview(params[0]);
                return convertToList(result);
            } catch (Exception e) {
                Log.e("error","JSON exception error");
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Review> list) {
            progressDialog.dismiss();
            if(list==null){
                Toast.makeText(BusinessDetailActivity.this,
                        "There is a JSON exception during searching process." ,
                        Toast.LENGTH_LONG
                ).show();
            }else{
                activityWeakReference.get().reviewsList.clear();
                activityWeakReference.get().reviewsList.addAll(list);
                activityWeakReference.get().hintText.setVisibility(View.GONE);
                activityWeakReference.get().reviewAdapter.notifyDataSetChanged();
            }
        }

        private List<Review> convertToList(JSONObject result) throws JSONException{
            List<Review> res = new ArrayList<>();
            JSONArray reviews = result.getJSONArray("reviews");
            for (int i = 0; i < reviews.length(); i++) {
                JSONObject temp = (JSONObject) reviews.get(i);
                Review review = new Review();
                JSONObject temp2=temp.getJSONObject("user");
                if(temp2!=null){
                    review.setName(temp2.getString("name"));
                }
                if(temp.get("text")!=null)
                    review.setContent(temp.getString("text"));
                if(temp.get("time_created")!=null)
                    review.setTime(temp.getString("time_created"));
                res.add(review);
            }
            return res;
        }
    }

    private void setColor(){
        String[] colors=getResources().getStringArray(R.array.colors);
        SharedPreferences colorSettings=getSharedPreferences("colors",0);
        int id=colorSettings.getInt("color", -1);
        if(id==-1){
            //initialize color
            SharedPreferences.Editor editor=colorSettings.edit();
            editor.putInt("color", 0);
            editor.commit();
            id=0;
        }
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor(colors[id]));
        getWindow().setBackgroundDrawable(colorDrawable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setColor();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_business_detail, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        int i=getIntent().getIntExtra("menu",0);
        if(i==0){
            menu.getItem(1).setEnabled(false);
            menu.getItem(1).setVisible(false);
        }else if(i==1){
            menu.getItem(0).setEnabled(false);
            menu.getItem(0).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menu_add: {
                repository.insertBusiness(business);
                break;
            }
            case R.id.menu_delete: {
                repository.deleteBusiness(business);
                finish();
                break;
            }
            default:{
                break;
            }
        }
        return true;
    }
}
