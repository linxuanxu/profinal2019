package linxuanxu.profinal2019.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import linxuanxu.profinal2019.R;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button_yelp_fusion)
    Button button1;

    @BindView(R.id.button_saved_business)
    Button button2;

    @BindView(R.id.button_settings)
    Button button3;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Main Page");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);

        ButterKnife.bind(this);
        setColor();

        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(MainActivity.this, BusinessSearchActivity.class);
                startActivity(mIntent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(MainActivity.this, SavedBusinessActivity.class);
                startActivity(mIntent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(mIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menu_yelp_fusion: {
                Intent mIntent = new Intent(MainActivity.this, BusinessSearchActivity.class);
                startActivity(mIntent);
                break;
            }
            case R.id.menu_saved_business:{
                Intent mIntent = new Intent(MainActivity.this, SavedBusinessActivity.class);
                startActivity(mIntent);
                break;
            }
            case R.id.menu_settings:{
                Intent mIntent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(mIntent);
                break;
            }
            case R.id.menu_exit: {
                finish();
                System.exit(0);
                break;
            }
            default:{
                break;
            }
        }
        return true;
    }

    private void setColor(){
        String[] colors=getResources().getStringArray(R.array.colors);
        SharedPreferences colorSettings=getSharedPreferences("colors",0);
        int id=colorSettings.getInt("color", -1);
        if(id==-1){
            //initialize color
            SharedPreferences.Editor editor=colorSettings.edit();
            editor.putInt("color", 0);
            editor.commit();
            id=0;
        }
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor(colors[id]));
        getWindow().setBackgroundDrawable(colorDrawable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setColor();
    }
}
