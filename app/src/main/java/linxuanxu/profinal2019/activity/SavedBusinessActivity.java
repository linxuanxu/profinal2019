package linxuanxu.profinal2019.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import linxuanxu.profinal2019.R;
import linxuanxu.profinal2019.adapter.BusinessAdapter;
import linxuanxu.profinal2019.model.Business;
import linxuanxu.profinal2019.viewmodel.SavedBusinessViewModel;

public class SavedBusinessActivity extends AppCompatActivity implements BusinessAdapter.OnBusinessClick{

    private SavedBusinessViewModel viewModel;

    @BindView(R.id.recyclerview_saved_business)
    RecyclerView recyclerView;

    private List<Business> businessList=new ArrayList<>();

    private BusinessAdapter businessAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_business);
        setTitle("Saved Business");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_saved_business);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        initRecyclerView();
        initViewModel();
    }

    private void initRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(SavedBusinessActivity.this));
        businessAdapter=new BusinessAdapter(SavedBusinessActivity.this, businessList);
        recyclerView.setAdapter(businessAdapter);
    }

    private void initViewModel(){
        final Observer<List<Business>> businessesObserver =
                new Observer<List<Business>>() {
                    @Override
                    public void onChanged(@Nullable List<Business> businesses) {
                        businessList.clear();
                        businessList.addAll(businesses);
                        if (businessAdapter == null) {
                            businessAdapter = new BusinessAdapter(SavedBusinessActivity.this,businessList);
                            recyclerView.setAdapter(businessAdapter);
                        } else {
                            businessAdapter.notifyDataSetChanged();
                        }
                    }
                };
        viewModel = ViewModelProviders.of(this).get(SavedBusinessViewModel.class);
        viewModel.business.observe(this, businessesObserver);
    }

    private void setColor(){
        String[] colors=getResources().getStringArray(R.array.colors);
        SharedPreferences colorSettings=getSharedPreferences("colors",0);
        int id=colorSettings.getInt("color", -1);
        if(id==-1){
            //initialize color
            SharedPreferences.Editor editor=colorSettings.edit();
            editor.putInt("color", 0);
            editor.commit();
            id=0;
        }
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor(colors[id]));
        getWindow().setBackgroundDrawable(colorDrawable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setColor();
    }

    @Override
    public void onBusinessClick(int pos) {
        Intent intent=new Intent(SavedBusinessActivity.this, BusinessDetailActivity.class);
        intent.putExtra("business",businessList.get(pos));
        intent.putExtra("menu",1);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_saved_business, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menu_delete_all: {
                viewModel.deleteAllBusinesses();
                break;
            }
            default:{
                break;
            }
        }
        return true;
    }
}
