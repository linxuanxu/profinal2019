package linxuanxu.profinal2019.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import linxuanxu.profinal2019.R;
import linxuanxu.profinal2019.adapter.BusinessAdapter;
import linxuanxu.profinal2019.model.Business;
import linxuanxu.profinal2019.model.Location;
import linxuanxu.profinal2019.util.YelpAPI;

public class BusinessSearchActivity extends AppCompatActivity implements BusinessAdapter.OnBusinessClick{
    @BindView(R.id.spinner_term)
    Spinner spinnerTerm;
    @BindView(R.id.spinner_location)
    Spinner spinnerLocation;
    @BindView(R.id.spinner_price)
    Spinner spinnerPrice;

    @BindView(R.id.search_button)
    Button button;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.search_hint)
    TextView searchHintText;

    String[] terms;
    String[] locations;
    String[] prices;

    YelpAPI api;
    String key;

    private List<Business> businessList=new ArrayList<>();
    private BusinessAdapter businessAdapter;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_search);
        Toolbar toolbar = findViewById(R.id.toolbar_yelp);
        toolbar.setTitle("Yelp Fusion");
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        setColor();
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                search();
            }
        });
        initAPI();
        initSpinner();
        initRecyclerView();
    }

    private void search(){
        try {
            String term = spinnerTerm.getSelectedItem().toString();
            String location = spinnerLocation.getSelectedItem().toString();
            String priceStr=spinnerPrice.getSelectedItem().toString();
            if(term==null||location==null||priceStr==null||term.equals("")||location.equals("")||priceStr.equals("")){
                Toast.makeText(BusinessSearchActivity.this,
                        "Something wrong about the search conditions." ,
                        Toast.LENGTH_LONG
                ).show();
                return;
            }
            String price="";
            switch (priceStr) {
                case "$":
                    price="1";
                    break;
                case "$$":
                    price="2";
                    break;
                case "$$$":
                    price="3";
                    break;
                case "$$$$":
                    price="4";
                    break;
            }
            if(price.equals("")){
                Toast.makeText(BusinessSearchActivity.this,
                        "Something wrong about the search conditions." ,
                        Toast.LENGTH_LONG
                ).show();
                return;
            }
            new BusinessSearchTask(BusinessSearchActivity.this).execute(new String[]{term,location,price});
        }catch (Exception e){
            Toast.makeText(BusinessSearchActivity.this,
                    "Something wrong about the search conditions." ,
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    private void initAPI(){
        key=getResources().getString(R.string.yelp_fusioin_key);
        if(key!=null){
            api=new YelpAPI(key);
        }
    }

    private void initSpinner(){
        terms=getResources().getStringArray(R.array.terms);
        locations=getResources().getStringArray(R.array.locations);
        prices=getResources().getStringArray(R.array.prices);
        ArrayAdapter<String> adapter1=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,terms);
        ArrayAdapter<String> adapter2=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);
        ArrayAdapter<String> adapter3=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,prices);
        spinnerTerm.setAdapter(adapter1);
        spinnerLocation.setAdapter(adapter2);
        spinnerPrice.setAdapter(adapter3);
    }

    private void initRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(BusinessSearchActivity.this));
        businessAdapter=new BusinessAdapter(BusinessSearchActivity.this, businessList);
        recyclerView.setAdapter(businessAdapter);
    }

    private class BusinessSearchTask extends AsyncTask<String, Void, List<Business>> {
        private ProgressDialog progressDialog;
        private WeakReference<BusinessSearchActivity> activityWeakReference;

        BusinessSearchTask(BusinessSearchActivity context){
            activityWeakReference=new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(BusinessSearchActivity.this);
            progressDialog.setTitle("Searching...");
            progressDialog.setMessage("One moment please...");
            progressDialog.setCancelable(true);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            BusinessSearchTask.this.cancel(true);
                            progressDialog.dismiss();
                        }
                    });
            progressDialog.show();
        }
        @Override
        protected List<Business> doInBackground(String... params) {
            if(api==null){
                return null;
            }
            try {
                JSONObject result= api.getBusiness(params[0],params[1],params[2]);
                return convertToList(result);
            } catch (Exception e) {
                Log.e("error","JSON exception error");
                return null;
            }
        }
        @Override
        protected void onPostExecute(List<Business> list) {
            progressDialog.dismiss();
            if(list==null){
                Toast.makeText(BusinessSearchActivity.this,
                        "There is a JSON exception during searching process." ,
                        Toast.LENGTH_LONG
                ).show();
            }else{
                activityWeakReference.get().businessList.clear();
                activityWeakReference.get().businessList.addAll(list);
                activityWeakReference.get().searchHintText.setVisibility(View.GONE);
                activityWeakReference.get().businessAdapter.notifyDataSetChanged();
            }
        }

        private List<Business> convertToList(JSONObject result) throws JSONException {
            List<Business> res=new ArrayList<>();
            JSONArray businesses=result.getJSONArray("businesses");
            for(int i=0;i<businesses.length();i++){
                JSONObject temp=(JSONObject) businesses.get(i);
                Business business=new Business();
                business.setId(temp.getString("id"));
                business.setName(temp.getString("name"));
                business.setPrice(temp.getString("price"));
                business.setRating(temp.getInt("rating"));
                business.setReview_count(temp.getInt("review_count"));
                business.setPhone(temp.getString("phone"));
                JSONObject temp2=temp.getJSONObject("location");
                if(temp2!=null){
                    Location location=new Location();
                    location.setAddress1(temp2.getString("address1"));
                    location.setAddress2(temp2.getString("address2"));
                    location.setAddress3(temp2.getString("address3"));
                    location.setCity(temp2.getString("city"));
                    location.setState(temp2.getString("state"));
                    location.setCountry(temp2.getString("country"));
                    location.setZip_code(temp2.getString("zip_code"));
                    business.setLocation(location);
                }
                res.add(business);
            }
            return res;
        }
    }

    @Override
    public void onBusinessClick(int pos) {
        Intent intent=new Intent(BusinessSearchActivity.this, BusinessDetailActivity.class);
        intent.putExtra("business",businessList.get(pos));
        intent.putExtra("menu",0);
        startActivity(intent);
    }

    private void setColor(){
        String[] colors=getResources().getStringArray(R.array.colors);
        SharedPreferences colorSettings=getSharedPreferences("colors",0);
        int id=colorSettings.getInt("color", -1);
        if(id==-1){
            //initialize color
            SharedPreferences.Editor editor=colorSettings.edit();
            editor.putInt("color", 0);
            editor.commit();
            id=0;
        }
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor(colors[id]));
        getWindow().setBackgroundDrawable(colorDrawable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setColor();
    }
}
