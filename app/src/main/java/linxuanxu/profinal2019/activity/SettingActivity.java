package linxuanxu.profinal2019.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import linxuanxu.profinal2019.R;

public class SettingActivity extends AppCompatActivity {

    @BindView(R.id.button_change_color)
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = findViewById(R.id.toolbar_settings);
        toolbar.setTitle("Settings");
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        setColor();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] colors=getResources().getStringArray(R.array.colors);
                SharedPreferences colorSettings=getSharedPreferences("colors",0);
                SharedPreferences.Editor editor=colorSettings.edit();
                int id=colorSettings.getInt("color", 0);
                id++;
                if(id>=colors.length){
                    id=0;
                }
                editor.putInt("color", id);
                editor.commit();
                setColor();
            }
        });
    }

    private void setColor(){
        String[] colors=getResources().getStringArray(R.array.colors);
        SharedPreferences colorSettings=getSharedPreferences("colors",0);
        int id=colorSettings.getInt("color", -1);
        if(id==-1){
            //initialize color
            SharedPreferences.Editor editor=colorSettings.edit();
            editor.putInt("color", 0);
            editor.commit();
            id=0;
        }
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor(colors[id]));
        getWindow().setBackgroundDrawable(colorDrawable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setColor();
    }
}
