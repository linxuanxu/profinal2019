package linxuanxu.profinal2019.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import linxuanxu.profinal2019.R;
import linxuanxu.profinal2019.model.Business;

public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.BusinessViewHolder> {
    private LayoutInflater mInflater;
    private List<Business> businessList;
    private OnBusinessClick onBusinessClick;


    public interface OnBusinessClick{
        void onBusinessClick(int pos);
    }

    public class BusinessViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView business_name;

        private BusinessViewHolder(View itemView){
            super(itemView);
            itemView.setOnClickListener(this);
            business_name=itemView.findViewById(R.id.row_text);
        }

        public void onClick(View v){
            onBusinessClick.onBusinessClick(getAdapterPosition());
        }
    }

    public BusinessAdapter(Context context, List<Business> list){
        mInflater= LayoutInflater.from(context);
        businessList=list;
        onBusinessClick=(OnBusinessClick) context;
    }

    public BusinessViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView=mInflater.inflate(R.layout.recyclerview_item_business_search,parent, false);
        return new BusinessViewHolder(itemView);
    }

    public void onBindViewHolder(BusinessViewHolder holder, int pos){
        if(businessList!=null){
            Business b=businessList.get(pos);
            holder.business_name.setText(b.getName());
        }else{
            //Exception:
            //task list isn't initialized
            Log.e("error","task list isn't initialized");
        }
    }

    public int getItemCount() {
        if (businessList != null){
            return businessList.size();
        }else{
            //Exception:
            //business list isn't initialized
            Log.e("error","business list isn't initialized");
            return 0;
        }
    }
}
