package linxuanxu.profinal2019.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import linxuanxu.profinal2019.database.BusinessRepository;
import linxuanxu.profinal2019.model.Business;

public class SavedBusinessViewModel extends AndroidViewModel {
    public LiveData<List<Business>> business;
    private BusinessRepository repository;

    public SavedBusinessViewModel(@NonNull Application application) {
        super(application);
        repository=BusinessRepository.getInstance(application.getApplicationContext());
        business=repository.business;
    }

    public void deleteAllBusinesses(){
        repository.deleteAllBusinesses();
    }
}
